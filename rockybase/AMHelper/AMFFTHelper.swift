//
//  AMFFTHelper.swift
//  MicToFrequency
//
//  Created by owen on 2016/11/1.
//  Copyright © 2016年 Ampacs. All rights reserved.
//

import Foundation
import Accelerate

class AMFFTHelper {
    /* static fields */
    class var kLevelLength:Int { get { return 128 } }
    private final var kAdjust0DB: Float = 1.5849e-13
    
    /* private fields */
    fileprivate let fft:FFTSetup
    fileprivate var fftBufferLength:Int
    fileprivate var fftNormalFactor:Float
    
    fileprivate let fftLog2n:vDSP_Length
    fileprivate let fftLength:vDSP_Length
    
    fileprivate var dspSplitComplex:DSPSplitComplex
    fileprivate var fftBuffer:UnsafeMutablePointer<Float>
    fileprivate var fftSpectrum:UnsafeMutablePointer<Float>
    
    init(_ fftBufferLength:Int){
        self.fftLog2n           = vDSP_Length(log2Ceil(UInt32(fftBufferLength)))
        self.fftBufferLength    = 2 << Int(self.fftLog2n-1)
        self.fftNormalFactor    = 1.0/Float(2*self.fftBufferLength)
        self.fftLength          = vDSP_Length(self.fftBufferLength/2)
        
        self.fftBuffer = UnsafeMutablePointer<Float>.allocate(capacity: self.fftBufferLength)
        self.fftSpectrum = UnsafeMutablePointer<Float>.allocate(capacity: Int(self.fftLength))
        
        self.dspSplitComplex = DSPSplitComplex(
            realp: UnsafeMutablePointer<Float>.allocate(capacity: Int(self.fftLength)),
            imagp: UnsafeMutablePointer<Float>.allocate(capacity: Int(self.fftLength)))
        
        self.fft = vDSP_create_fftsetup(self.fftLog2n, FFTRadix(kFFTRadix2))!
    }
    
    deinit{
        free(self.dspSplitComplex.realp)
        free(self.dspSplitComplex.imagp)
        free(self.fftBuffer)
        free(self.fftSpectrum)
        
        vDSP_destroy_fftsetup(self.fft)
    }
    
    /* private methods */
    fileprivate func process(){
        var scale:Float = self.fftNormalFactor
        
        self.fftBuffer.withMemoryRebound(to: DSPComplex.self, capacity: Int(self.fftLength)) {
            inDataPtr in
            vDSP_ctoz(inDataPtr, 2, &self.dspSplitComplex, 1, self.fftLength)
        }
        
        vDSP_fft_zrip(self.fft, &self.dspSplitComplex, 1, self.fftLog2n, FFTDirection(kFFTDirection_Forward))
        vDSP_vsmul(self.dspSplitComplex.realp, 1, &scale, self.dspSplitComplex.realp, 1, self.fftLength)
        vDSP_vsmul(self.dspSplitComplex.imagp, 1, &scale, self.dspSplitComplex.imagp, 1, self.fftLength)
        
        //Convert the fft data to dB
        vDSP_zvabs(&self.dspSplitComplex, 1, self.fftSpectrum, 1, self.fftLength)
        vDSP_vsdiv(self.fftSpectrum, 1, &scale, self.fftSpectrum, 1, self.fftLength)
    }
    
    fileprivate func normalize(){
        let stride = Float(self.fftLength)/Float(AMFFTHelper.kLevelLength)
        
        for i in 0..<AMFFTHelper.kLevelLength {
            var val = self.fftSpectrum[Int(Float(i)*stride)]
            
            if val < 0 { val = 0 }
            if val > 1 { val = 1 }
            self.levels[i] = val
        }
    }
    
    /* public properties */
    var levels:[Float] = Array<Float>.init(repeating: 0, count: AMFFTHelper.kLevelLength)
    
    /* public methods */
    func executeFFT(_ sample:UnsafePointer<Float>, numFrames:Int){
        vDSP_vclr(self.fftBuffer, 1, vDSP_Length(self.fftBufferLength))
        
        let length = numFrames > self.fftBufferLength ? self.fftBufferLength : numFrames
        
        for i in 0..<length {
            self.fftBuffer[i] = sample[i]
        }
        
        self.process()
        self.normalize()
    }
    func resetLevels(){
        memset(self.fftBuffer, 0, self.fftBufferLength)
    }
}
