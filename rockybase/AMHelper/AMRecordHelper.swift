//
//  AMRecordHelper.swift
//  MicToFrequency
//
//  Created by owen on 2016/11/1.
//  Copyright © 2016年 Ampacs. All rights reserved.
//

import AudioToolbox
import AVFoundation

class AMRecordHelper {
    fileprivate var _rioUnit:AudioUnit? = nil
    fileprivate var _fftHelper:AMFFTHelper? = nil
    
    fileprivate let _renderCallback:AURenderCallback = {
        (inRefCon,
        ioActionFlags/*: UnsafeMutablePointer<AudioUnitRenderActionFlags>*/,
        inTimeStamp/*: UnsafePointer<AudioTimeStamp>*/,
        inBufNumber/*: UInt32*/,
        inNumberFrames/*: UInt32*/,
        ioData/*: UnsafeMutablePointer<AudioBufferList>*/)
        -> OSStatus
        in
        
        let helper = unsafeBitCast(inRefCon, to: AMRecordHelper.self)
        
        return helper.renderCallbackFunc(ioActionFlags, inTimeStamp, inBufNumber, inNumberFrames, ioData!)
    }
    
    init(){
        self.setupAudioChain()
    }
    
    fileprivate func setupAudioChain(){
        self.setupAudioSession()
        self.setupIOUnit()
    }
    
    fileprivate func setupAudioSession(){
        
        // Configure the audio session
        let sessionInstance = AVAudioSession.sharedInstance()
        
        // we are going to play and record so we pick that category
        do {
            try sessionInstance.setCategory(AVAudioSessionCategoryPlayAndRecord)
        } catch _ as NSError {
            fatalError("couldn't set session's audio category")
        } catch {
            fatalError()
        }
        
        // set the buffer duration to 5 ms
        let bufferDuration: TimeInterval = 0.005
        do {
            try sessionInstance.setPreferredIOBufferDuration(bufferDuration)
        } catch _ as NSError {
            fatalError("couldn't set session's I/O buffer duration")
        } catch {
            fatalError()
        }
        
        do {
            // set the session's sample rate
            try sessionInstance.setPreferredSampleRate(44100)
        } catch _ as NSError {
            fatalError("couldn't set session's preferred sample rate")
        } catch {
            fatalError()
        }
        
        do {
            // activate the audio session
            try sessionInstance.setActive(true)
        } catch _ as NSError {
            fatalError("couldn't set session active")
        } catch {
            fatalError()
        }
    }
    
    fileprivate func setupIOUnit(){
        do {
            var desc = AudioComponentDescription(
                componentType: OSType(kAudioUnitType_Output),
                componentSubType: OSType(kAudioUnitSubType_RemoteIO),
                componentManufacturer: OSType(kAudioUnitManufacturer_Apple),
                componentFlags: 0,
                componentFlagsMask: 0)
            
            let comp = AudioComponentFindNext(nil, &desc)
            
            try __exceptionIfError(AudioComponentInstanceNew(comp!, &self._rioUnit),"couldn't create a new instance of AURemoteIO")
            
            var one:UInt32 = 1
            
            try __exceptionIfError(AudioUnitSetProperty(self._rioUnit!, kAudioOutputUnitProperty_EnableIO, kAudioUnitScope_Input, 1, &one, __sizeOf32(one)),"could't enable input on AURemoteIO")
            
            try __exceptionIfError(AudioUnitSetProperty(self._rioUnit!, kAudioOutputUnitProperty_EnableIO, kAudioUnitScope_Output, 0, &one, __sizeOf32(one)),"could't enable output on AURemoteIO")
            
            var ioFormat = CAStreamBasicDescription(sampleRate: 44100, numChannels: 1, pcmf: .float, isInterleaved: false)
            
            try __exceptionIfError(AudioUnitSetProperty(self._rioUnit!, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Output, 1, &ioFormat, __sizeOf32(ioFormat)), "couldn't set the output client format on AURemoteIO")
            
            try __exceptionIfError(AudioUnitSetProperty(self._rioUnit!, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Input, 0, &ioFormat, __sizeOf32(ioFormat)), "couldn't set the input client format on AURemoteIO")
            
            var maxFramesPerSlice:UInt32 = 256
            
            //try __exceptionIfError(AudioUnitSetProperty(self._rioUnit!, kAudioUnitProperty_MaximumFramesPerSlice, kAudioUnitScope_Global, 0, &maxFramesPerSlice, __sizeOf32(UInt32.self)), "couldn't set max frames per slice on AURemoteIO")

            var propSize = __sizeOf32(UInt32.self)
            try __exceptionIfError(AudioUnitGetProperty(
                self._rioUnit!, kAudioUnitProperty_MaximumFramesPerSlice, kAudioUnitScope_Global, 0, &maxFramesPerSlice, &propSize), "couldn't get max frames per slice on AURemoteIO")
            
            self.bufferSize = Int(maxFramesPerSlice)
            
            var renderCallback = AURenderCallbackStruct(inputProc: self._renderCallback, inputProcRefCon: Unmanaged.passUnretained(self).toOpaque())
            
            try __exceptionIfError(AudioUnitSetProperty(self._rioUnit!, kAudioUnitProperty_SetRenderCallback, kAudioUnitScope_Input, 0, &renderCallback, __sizeOf32(renderCallback)),
                           "couldn't set render callback on AURemoteIO")
            
            try __exceptionIfError(AudioUnitInitialize(self._rioUnit!), "couldn't initialize AURemoteIO instance")
        } catch let e as __Exception {
            fatalError(e.description)
        } catch _ {
            fatalError()
        }
    }
    
    fileprivate func renderCallbackFunc(_ ioActionFlags: UnsafeMutablePointer<AudioUnitRenderActionFlags>,
                                        _ inTimeStamp: UnsafePointer<AudioTimeStamp>,
                                        _ inBufNumber: UInt32,
                                        _ inNumberFrames: UInt32,
                                        _ ioData: UnsafeMutablePointer<AudioBufferList>) -> OSStatus {
        
        let ioPtr = UnsafeMutableAudioBufferListPointer(ioData)
        var err: OSStatus = noErr
        
        // we are calling AudioUnitRender on the input bus of AURemoteIO
        // this will store the audio data captured by the microphone in ioData
        err = AudioUnitRender(self._rioUnit!, ioActionFlags, inTimeStamp, 1, inNumberFrames, ioData)
        
        
        if self._fftHelper != nil {
            self._fftHelper?.executeFFT(ioPtr[0].mData!.assumingMemoryBound(to: Float.self), numFrames: Int(inNumberFrames))
        }
        
        for i in 0..<ioPtr.count {
            memset(ioPtr[i].mData, 0, Int(ioPtr[i].mDataByteSize))
        }
        
        return err
    }
    
    /* public properties */
    
    var fftHelper:AMFFTHelper? {
        set(val){
            self._fftHelper = val
        }
        get{
            return self._fftHelper
        }
    }
    
    var bufferSize:Int = 0
    
    /* public methods */
    func start() {
        let err = AudioOutputUnitStart(self._rioUnit!)
        if err != 0 {
            print("could't start AURemoteIO: %d", Int32(err))
        }
    }
    
    func stop() {
        let err = AudioOutputUnitStop(self._rioUnit!)
        if err != 0 {
            print("could't stop AURemoteIO: %d", Int32(err))
        }
    }
}
