//
//  AMHelperUtils.swift
//  MicToFrequency
//
//  Created by owen on 2016/11/2.
//  Copyright © 2016年 Ampacs. All rights reserved.
//

import Foundation

internal class __Throwable : CustomStringConvertible, Error {

    private(set) var message:String
    private(set) var cause:__Throwable?
    private(set) var file:String
    private(set) var function:String
    private(set) var line:Int

    init(message:String = "", cause: __Throwable? = nil,
         file:String = #file,
         function:String = #function,
         line:Int = #line, column:Int = #column) {

        self.message = message
        self.cause = cause
        self.file = file
        self.function = function
        self.line = line
    }

    public var description:String{
        return "\(message) in \(function) of \(file):\(line)"
    }
}

internal class __Exception:__Throwable{
    fileprivate var exceptionMessage:String
    fileprivate var error:OSStatus

    init(message:String?, error:OSStatus){
        self.exceptionMessage = message == nil ? "" : message!
        self.error = error
    }

    override var description: String {
        return "Error \(error) in \(exceptionMessage)"
    }
}

internal func __exceptionIfError(_ status:OSStatus, _ message:String) throws {
    if status != 0 {
        throw __Exception(message: message, error:status)
    }
}

internal func __sizeOf32<T>(_ X:T)->UInt32 { return UInt32(MemoryLayout<T>.size) }
