//
//  AncSettingsViewController.swift
//  rockybase
//
//  Created by owen on 2016/11/11.
//  Copyright © 2016年 Ampacs. All rights reserved.
//

import UIKit

class AncSettingsViewController: UITableViewController, UITextFieldDelegate {
    @IBOutlet weak var ancSwitch: UISwitch!

    @IBOutlet weak var fflTestSetAEdit:UITextField!
    @IBOutlet weak var ffrTestSetAEdit:UITextField!
    @IBOutlet weak var fblTestSetAEdit:UITextField!
    @IBOutlet weak var fbrTestSetAEdit:UITextField!
    
    @IBOutlet weak var fflTestSetBEdit:UITextField!
    @IBOutlet weak var ffrTestSetBEdit:UITextField!
    @IBOutlet weak var fblTestSetBEdit:UITextField!
    @IBOutlet weak var fbrTestSetBEdit:UITextField!
    
    @IBOutlet weak var linkSwitch: UISwitch!
    
    @IBOutlet weak var awareLeft: UISlider!
    
    @IBOutlet weak var awareRight: UISlider!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let edits = [fflTestSetAEdit, ffrTestSetAEdit, fblTestSetAEdit, fbrTestSetAEdit, fflTestSetBEdit, ffrTestSetBEdit, fblTestSetBEdit, fbrTestSetBEdit]
        
        for edit in edits {
            edit?.delegate = self
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /* UITextFieldDelegate */
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    
    /* IBActions */
    
    @IBAction func refreshClick(_ sender: Any) {
    }
    
    
    @IBAction func testASetClick(_ sender: Any) {
    }
    
    @IBAction func testBSetClick(_ sender: Any) {
    }
    
    
    @IBAction func writeToFlashClick(_ sender: Any) {
    }
    
    @IBAction func linkSwitchValueChanged(_ sender: UISwitch) {
    }
    
    @IBAction func leftValueChanged(_ sender: UISlider) {
    }
    
    @IBAction func rightValueChanged(_ sender: UISlider) {
    }
    
}
