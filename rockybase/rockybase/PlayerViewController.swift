//
//  PlayerViewController.swift
//  rockybase
//
//  Created by owen on 2016/10/24.
//  Copyright © 2016年 Ampacs. All rights reserved.
//

import UIKit
import MediaPlayer

class PlayerViewController: UIViewController {
    @IBOutlet weak var artworkView: UIImageView!
    
    @IBOutlet weak var timeControl: UISlider!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var songInfo: UILabel!
    
    @IBOutlet weak var resumeButton: UIImageView!
    
    @IBOutlet weak var prevButton: UIImageView!
    
    @IBOutlet weak var nextButton: UIImageView!
    
    @IBOutlet weak var shuffleButton: UIImageView!
    
    @IBOutlet weak var loopButton: UIImageView!
    
    @IBOutlet weak var mpVolumeViewRef: UIView!
    
    fileprivate var volumeView: MPVolumeView!
    
    fileprivate var player:AMPlayer!
    fileprivate var playing:Bool = false
    fileprivate var loopMode:AMPlayer.LoopMode = .noLoop
    fileprivate var shuffle:Bool = false
    
    fileprivate var delegateAct:AMPlayerDelegate!
    
    fileprivate var index:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.volumeView = MPVolumeView(frame: CGRect(x: 0, y: 0, width: 300, height: 24))
        self.volumeView.translatesAutoresizingMaskIntoConstraints = false
        self.volumeView.showsRouteButton = false
        self.mpVolumeViewRef.translatesAutoresizingMaskIntoConstraints = false
        self.mpVolumeViewRef.addSubview(volumeView)
        
        self.timeControl.setThumbImage(UIImage(named: "dot"), for: UIControlState.normal)
        self.timeControl.setThumbImage(UIImage(named: "dot"), for: UIControlState.selected)
        self.timeControl.isContinuous = true
        self.timeControl.value = 0
        
        var tap:UITapGestureRecognizer
        
        tap = UITapGestureRecognizer(target: self, action: #selector(self.onResumeClick))
        tap.numberOfTapsRequired = 1
        
        self.resumeButton.isUserInteractionEnabled = true
        self.resumeButton.addGestureRecognizer(tap)
        
        tap = UITapGestureRecognizer(target: self, action: #selector(self.onPrevClick))
        tap.numberOfTapsRequired = 1
        
        self.prevButton.isUserInteractionEnabled = true
        self.prevButton.addGestureRecognizer(tap)
        
        tap = UITapGestureRecognizer(target: self, action: #selector(self.onNextClick))
        tap.numberOfTapsRequired = 1
        
        self.nextButton.isUserInteractionEnabled = true
        self.nextButton.addGestureRecognizer(tap)
        
        tap = UITapGestureRecognizer(target: self, action: #selector(self.onShuffleClick))
        tap.numberOfTapsRequired = 1
        
        self.shuffleButton.isUserInteractionEnabled = true
        self.shuffleButton.addGestureRecognizer(tap)
        
        tap = UITapGestureRecognizer(target: self, action: #selector(self.onLoopClick))
        tap.numberOfTapsRequired = 1
        
        self.loopButton.isUserInteractionEnabled = true
        self.loopButton.addGestureRecognizer(tap)
        
        
        tap = UITapGestureRecognizer(target: self, action: #selector(self.onArtworkClick))
        tap.numberOfTapsRequired = 1
        
        self.artworkView.isUserInteractionEnabled = true
        self.artworkView.addGestureRecognizer(tap)
        
        self.player = AMPlayer.shareInstance
        self.delegateAct = AMPlayerDelegate()
        self.delegateAct.updateCurrentTime = {
            (currentTime) in
            DispatchQueue.main.async(execute: {
                self.timeControl.value = self.player.percentage
            })
        }
        self.delegateAct.onStart = {
            self.updateViews()
        }
        self.delegateAct.onFinish = {
            self.updateViews()
            self.timeControl.value = self.player.percentage
        }
        
        self.player.addDelegate(self.delegateAct)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        MPMediaLibrary.requestAuthorization({
            (status) in
            if status == .authorized {
                let songs = MPMediaQuery.songs().items
                if songs != nil {
                    AMPlayer.shareInstance.track = MPMediaItemCollection(items: songs!)
                }else{
                    AMPlayer.shareInstance.track = MPMediaItemCollection()
                }
            }
            if(AMPlayer.shareInstance.trackIndex < 0) {
                AMPlayer.shareInstance.trackIndex = 0
                self.updateViews(true)
            }
        })
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.player.removeDelegate(self.delegateAct)
    }
    
    override func viewDidLayoutSubviews() {
        self.volumeView.frame = CGRect(x:0, y:0, width:self.mpVolumeViewRef.frame.width, height:self.mpVolumeViewRef.frame.height)
        self.artworkView.layer.shadowColor = UIColor.black.cgColor
        self.artworkView.layer.shadowOpacity = 1
        self.artworkView.layer.shadowOffset = CGSize.zero
        self.artworkView.layer.shadowRadius = 10
        self.artworkView.layer.shadowPath = UIBezierPath(rect: self.artworkView.bounds).cgPath
        self.artworkView.layer.shouldRasterize = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func onResumeClick(_ sender:UITapGestureRecognizer){
        if sender.state != .ended { return }
        self.player.toggleResume()
        
        self.updateViews()
    }
    
    func onPrevClick(_ sender:UITapGestureRecognizer){
        if sender.state != .ended { return }
        self.player.prev()
        
        self.updateViews()
    }
    
    func onNextClick(_ sender:UITapGestureRecognizer){
        if sender.state != .ended { return }
        self.player.next()
        
        self.updateViews()
    }
    
    func onShuffleClick(_ sender:UITapGestureRecognizer){
        if sender.state != .ended { return }
        self.player.toggleShuffle()
        
        self.updateViews()
    }
    
    func onLoopClick(_ sender:UITapGestureRecognizer){
        if sender.state != .ended { return }
        self.player.toggleLoopMode()
        
        self.updateViews()
    }
    
    func onArtworkClick(_ sender:UITapGestureRecognizer){
        if sender.state != .ended { return }
    }
    
    @IBAction func timeChanged(_ sender: AnyObject) {
        self.player.percentage = self.timeControl.value
    }
    
    fileprivate func updateViews(_ forced:Bool=false){
        DispatchQueue.main.async(execute: {
            self.resumeButton.image = self.player.isPlaying ? UIImage(named:"control-pause"): UIImage(named:"control-play")
            self.shuffle = self.player.shuffle
            self.shuffleButton.alpha = self.player.shuffle ? 1.0 : 0.5
            self.loopMode = self.player.loopMode
            self.loopButton.alpha = (self.player.loopMode == AMPlayer.LoopMode.noLoop) ? 0.5 : 1.0
            if self.player.loopMode == AMPlayer.LoopMode.loopOne {
                self.loopButton.image = UIImage(named: "control-loop-once")
            } else {
                self.loopButton.image = UIImage(named: "control-loop")
            }
            self.timeControl.value = self.player.percentage
        })
        
        if self.index == self.player.trackIndex && !forced { return }
        self.index = self.player.trackIndex
        
        if self.player.mediaItem == nil { return }
        
        let mediaItem = self.player.mediaItem!
        
        let artwork:UIImage
        
        if mediaItem.artwork != nil {
            artwork = AMUtils.resizeImage(image:mediaItem.artwork!.image(at: CGSize(width: 250, height: 250))!, newWidth: 250)
        } else {
            artwork = AMUtils.resizeImage(image:UIImage(named: "music")!, newWidth:250)
        }
        DispatchQueue.main.async(execute: {
            self.titleLabel.text = mediaItem.title
            
            var songInfoText = mediaItem.artist
            if songInfoText == nil || songInfoText!.isEmpty {
                songInfoText = mediaItem.albumTitle
            } else {
                if !(mediaItem.albumTitle?.isEmpty)! {
                    songInfoText = songInfoText! + " - " + mediaItem.albumTitle!
                }
            }
            
            self.songInfo.text = songInfoText
            self.artworkView.image = artwork
        })
    }
    
    deinit {
        self.player.stop()
    }
}
