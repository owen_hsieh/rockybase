//
//  EqualizerViewController.swift
//  AmpacsMusic
//
//  Created by owen on 2016/9/13.
//  Copyright © 2016年 Ampacs. All rights reserved.
//

import UIKit

class EqualizerViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    @IBOutlet weak var eqPanelLabel: UILabel!
    
    @IBOutlet weak var equalizerView: EqualizerView!
    
    @IBOutlet weak var eqPresetPicker: UIPickerView!
    
    @IBOutlet weak var resetButton: UIButton!
    
    fileprivate var presets:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.presets = AMPlayer.shareInstance.equalizerPresets()
        
        self.eqPresetPicker.dataSource = self
        self.eqPresetPicker.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func onResetClick(_ sender: UIButton) {
        AMPlayer.shareInstance.resetEqualizer()
        self.equalizerView.updateView()
    }

    /* picker view impl */
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return presets.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        AMPlayer.shareInstance.equalizerPresetIndex = row
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return presets[row]
    }
}
