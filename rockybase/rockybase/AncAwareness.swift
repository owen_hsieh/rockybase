//
//  AncAwareness.swift
//  rockybase
//
//  Created by owen on 2016/11/1.
//  Copyright © 2016年 Ampacs. All rights reserved.
//

import UIKit

class AncAwareness : UIViewController {
    /* IBOutlets */
    @IBOutlet weak var ancSwitch:UISwitch!
    @IBOutlet weak var awarenessLeftSliderControl:UISlider!
    @IBOutlet weak var awarenessRightSliderControl:UISlider!
    @IBOutlet weak var awarenessLinkSwitchControl:UISlider!
    
    
    /* UIViewController overrides */
    override func viewDidLoad(){
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated:Bool){
        super.viewDidAppear(animated)
    }
    
    
    override func viewDidDisappear(_ animated:Bool){
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning(){
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /* IBActions */
    @IBAction func ancSwitchDidChange(_ sender:Any){}
    @IBAction func awarenessLeftSliderTouchUpInside(_ sender:Any){}
    @IBAction func awarenessLeftSliderTouchUpOutside(_ sender:Any){}
    @IBAction func awarenessRightSliderTouchUpInside(_ sender:Any){}
    @IBAction func awarenessRightSliderTouchUpOutside(_ sender:Any){}
    @IBAction func awarenessLinkTouchUpInside(_ sender:Any){}
    @IBAction func awarenessLinkTouchUpOutside(_ sender:Any){}
}
