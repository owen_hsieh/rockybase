//
//  UIImageProgressView.swift
//  rockybase
//
//  Created by owen on 2016/11/9.
//  Copyright © 2016年 Ampacs. All rights reserved.
//

import UIKit

class UIImageProgressView: UIImageView {

    fileprivate var __progress:CGFloat = 1
    fileprivate var __cgSize:CGSize!
    
    @IBInspectable var progress:CGFloat {
        set(value) {
            var newValue = value
            newValue = min(1, newValue)
            newValue = max(0, newValue)
            
            if newValue != __progress {
                __progress = newValue
                self.layoutSubviews()
            }
        }
        get {
            return self.__progress
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.__cgSize = self.frame.size
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.__cgSize = self.frame.size
    }
    
    override func layoutSubviews() {
        var newSize = self.__cgSize!
        newSize.width *= self.progress
        self.frame.size = newSize
    }
}
