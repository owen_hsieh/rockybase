//
//  AncIndicatorViewController.swift
//  rockybase
//
//  Created by owen on 2016/11/10.
//  Copyright © 2016年 Ampacs. All rights reserved.
//

import UIKit
import Charts

class AncIndicatorViewController: UITableViewController {
    @IBOutlet weak var ancSwitch: UISwitch!
    @IBOutlet weak var indicatorSwitch: UISwitch!
    
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var onLevel: UIImageProgressView!
    @IBOutlet weak var offLevel: UIImageProgressView!
    
    @IBOutlet weak var timeLineChart: LineChartView!
    
    @IBOutlet weak var frequencyLevelChart: LineChartView!
    
    fileprivate var timer:Timer!
    
    fileprivate var onEntries:[ChartDataEntry]!
    fileprivate var onEntriesIndex:Int = 0
    
    fileprivate var offEntries:[ChartDataEntry]!
    fileprivate var offEntriesIndex:Int = 0
    
    fileprivate var onLevels:[ChartDataEntry]!
    
    fileprivate var offLevels:[ChartDataEntry]!
    
    fileprivate var recordHelper:AMRecordHelper!
    fileprivate var fftHelper:AMFFTHelper!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.ancSwitch.isEnabled = false
        //self.indicatorSwitch.isEnabled = false
        self.levelLabel.text = ""
        self.offLevel.progress = 0
        self.onLevel.progress = 0
        
        self.offEntries = [ChartDataEntry()]
        self.onEntries = [ChartDataEntry()]
        
        self.initRecorder()
        self.initTimeLineChart()
        self.initFrequencyLevelChart()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    fileprivate func initRecorder(){
        self.recordHelper = AMRecordHelper()
        self.fftHelper = AMFFTHelper(self.recordHelper.bufferSize)
        self.recordHelper.fftHelper = self.fftHelper
    }
    
    fileprivate func initTimeLineChart(){
        /* chart setup */
        
        let ancOffSet:LineChartDataSet = LineChartDataSet(values: [], label: "ANC Off")
        
        ancOffSet.lineDashLengths = [5, 2.5]
        ancOffSet.highlightLineDashLengths = [5, 2.5]
        ancOffSet.setColor(UIColor.red)
        ancOffSet.setCircleColor(UIColor.red)
        ancOffSet.lineWidth = 1
        ancOffSet.circleRadius = 3
        ancOffSet.drawCircleHoleEnabled = false
        ancOffSet.valueFont = UIFont.systemFont(ofSize: 9)
        ancOffSet.formLineDashLengths = [5, 2.5]
        ancOffSet.formLineWidth = 1
        ancOffSet.formSize = 15
        
        ancOffSet.values = self.offEntries
        
        
        let ancOnSet:LineChartDataSet = LineChartDataSet(values: [], label: "ANC On")
        
        ancOnSet.lineDashLengths = [5, 2.5]
        ancOnSet.highlightLineDashLengths = [5, 2.5]
        ancOnSet.setColor(UIColor.green)
        ancOnSet.setCircleColor(UIColor.green)
        ancOnSet.lineWidth = 1
        ancOnSet.circleRadius = 3
        ancOnSet.drawCircleHoleEnabled = false
        ancOnSet.valueFont = UIFont.systemFont(ofSize: 9)
        ancOnSet.formLineDashLengths = [5, 2.5]
        ancOnSet.formLineWidth = 1
        ancOnSet.formSize = 15
        
        ancOnSet.values = self.onEntries
        
        self.timeLineChart.data = LineChartData(dataSets: [ancOffSet, ancOnSet])
        self.timeLineChart.data?.notifyDataChanged()
        self.timeLineChart.notifyDataSetChanged()
        self.timeLineChart.xAxis.drawLabelsEnabled = false
        self.timeLineChart.xAxis.axisMinimum = 0
        self.timeLineChart.xAxis.axisMaximum = 19
    }
    
    fileprivate func initFrequencyLevelChart(){
        /* chart setup */
        
        let ancOffSet:LineChartDataSet = LineChartDataSet(values: [], label: "ANC Off")
        
        ancOffSet.lineDashLengths = [5, 2.5]
        ancOffSet.highlightLineDashLengths = [5, 2.5]
        ancOffSet.setColor(UIColor.red)
        ancOffSet.lineWidth = 1
        ancOffSet.drawCirclesEnabled = false
        ancOffSet.valueFont = UIFont.systemFont(ofSize: 9)
        ancOffSet.formLineDashLengths = [5, 2.5]
        ancOffSet.formLineWidth = 1
        ancOffSet.formSize = 15
        
        ancOffSet.values = self.offEntries
        
        
        let ancOnSet:LineChartDataSet = LineChartDataSet(values: [], label: "ANC On")
        
        ancOnSet.lineDashLengths = [5, 2.5]
        ancOnSet.highlightLineDashLengths = [5, 2.5]
        ancOnSet.setColor(UIColor.green)
        ancOnSet.lineWidth = 1
        ancOnSet.drawCirclesEnabled = false
        ancOnSet.valueFont = UIFont.systemFont(ofSize: 9)
        ancOnSet.formLineDashLengths = [5, 2.5]
        ancOnSet.formLineWidth = 1
        ancOnSet.formSize = 15
        
        ancOnSet.values = self.onEntries
        
        self.frequencyLevelChart.data = LineChartData(dataSets: [ancOffSet, ancOnSet])
        self.frequencyLevelChart.data?.notifyDataChanged()
        self.frequencyLevelChart.notifyDataSetChanged()
        self.frequencyLevelChart.xAxis.drawLabelsEnabled = false
        self.frequencyLevelChart.leftAxis.drawLabelsEnabled = false
    }
    
    fileprivate func startIndicators(){
        if self.timer == nil {
            self.timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: {
                (_timer) in
                
                self.updateIndicators()
            })
        }
        self.recordHelper.start()
    }
    
    fileprivate func stopIndicators(){
        if self.timer != nil {
            self.timer.invalidate()
            self.timer = nil
        }
        self.recordHelper.stop()
    }
    
    fileprivate func updateIndicators(){
        if !self.indicatorSwitch.isOn { return }
        
        if self.ancSwitch.isOn {
            self.updateOnEntry(Double(arc4random_uniform(20)) + 30)
            self.updateOnLevels()
        }else{
            self.updateOffEntry(Double(arc4random_uniform(80)) + 60)
            self.updateOffLevels()
        }
    }
    
    fileprivate func updateOnEntry(_ newValue:Double){
        self.onEntriesIndex += 1
        
        self.levelLabel.text = String(newValue) + "dB"
        
        self.onLevel.progress = CGFloat(Float(newValue/140.0))
        
        if self.onEntries.count >= 20 {
            self.onEntries.remove(at: 0)
            for entry in self.onEntries {
                entry.x -= 1
            }
        }
        self.onEntries.append(ChartDataEntry(x: Double(self.onEntries.count), y: newValue))
        
        let set = self.timeLineChart.data?.dataSets[1] as! LineChartDataSet
        set.values = self.onEntries
        
        self.timeLineChart.data?.notifyDataChanged()
        self.timeLineChart.notifyDataSetChanged()
    }
    
    fileprivate func updateOffEntry(_ newValue:Double){
        self.levelLabel.text = String(newValue) + "dB"
        
        self.offLevel.progress = CGFloat(Float(newValue/140.0))
        
        if self.offEntries.count >= 20 {
            self.offEntries.remove(at: 0)
            for entry in self.offEntries {
                entry.x -= 1
            }
        }
        self.offEntries.append(ChartDataEntry(x: Double(self.offEntries.count), y: newValue))
        
        let set = self.timeLineChart.data?.dataSets[0] as! LineChartDataSet
        set.values = self.offEntries
        self.timeLineChart.data?.notifyDataChanged()
        self.timeLineChart.notifyDataSetChanged()
    }
    
    fileprivate func updateOnLevels(){
        let levels = self.fftHelper.levels
        
        self.onLevels = []
        
        for idx in 0..<levels.count {
            self.onLevels.append(ChartDataEntry(x:Double(idx), y:Double(levels[idx]*0.6)))
        }
        
        let set = self.frequencyLevelChart.data?.dataSets[1] as! LineChartDataSet
        set.values = self.onLevels
        self.frequencyLevelChart.data?.notifyDataChanged()
        self.frequencyLevelChart.notifyDataSetChanged()
    }
    
    fileprivate func updateOffLevels(){
        let levels = self.fftHelper.levels
        
        self.offLevels = []
        
        for idx in 0..<levels.count {
            self.offLevels.append(ChartDataEntry(x:Double(idx), y:Double(levels[idx])))
        }
        
        let set = self.frequencyLevelChart.data?.dataSets[0] as! LineChartDataSet
        set.values = self.offLevels
        self.frequencyLevelChart.data?.notifyDataChanged()
        self.frequencyLevelChart.notifyDataSetChanged()
    }
    
    @IBAction func indicatorSwitchValueChanged(_ sender: UISwitch) {
        if sender.isOn {
            startIndicators()
        }else{
            stopIndicators()
        }
    }
}
