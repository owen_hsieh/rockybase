//
//  EqualizerView.swift
//  AmpacsMusic
//
//  Created by owen on 2016/9/13.
//  Copyright © 2016年 Ampacs. All rights reserved.
//

import UIKit

class EqualizerView : UIScrollView{
    
    fileprivate var gains:[Float] = []
    fileprivate var sliders:[UISlider] = []
    fileprivate var labels:[UILabel] = []
    fileprivate var textFields:[UITextField] = []
    
    override init(frame:CGRect){
        super.init(frame:frame)
        self.initializeViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initializeViews()
    }
    
    fileprivate func initializeViews(){
        self.gains = AMPlayer.shareInstance.frequencyGain()
        let bands = AMPlayer.shareInstance.frequencyBand()
        
        for tag in 0..<bands.count {
            let slider:UISlider = UISlider(frame:CGRect.zero)
            slider.minimumValue = -12
            slider.maximumValue = 12
            slider.setThumbImage(UIImage(named:"dot"), for: UIControlState.normal)
            slider.tag = tag
            slider.addTarget(self, action: #selector(self.sliderValueDidChanged), for: .valueChanged)
            sliders.append(slider)
            self.addSubview(slider)
            
            let label:UILabel = UILabel()
            let frequency = Int(bands[tag])
            let text = frequency >= 1000 ? String(frequency/1000)+"K" : String(frequency)
            
            label.text = text
            label.textAlignment = .center
            labels.append(label)
            self.addSubview(label)
            
            let textField:UITextField = UITextField()
            let gain = gains[tag]
            
            textField.tag = tag
            textField.textAlignment = .center
            textField.keyboardType = .decimalPad
            textField.text = String(gain)
            textField.addTarget(self, action: #selector(self.textFieldValueDidEnd), for: .editingDidEnd)
            textField.borderStyle = .roundedRect
            textFields.append(textField)
            self.addSubview(textField)
        }
        
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.updateSliders()
        
        var cgSize = self.bounds.size
        cgSize.width = CGFloat(80 * self.sliders.count)
        self.contentSize = cgSize
    }
    
    fileprivate func updateSliders(){
        //let viewWidth = self.frame.size.width / CGFloat(self.sliders.count)
        let viewWidth = CGFloat(80)
        let viewHeight = self.frame.size.height
        let viewCount = self.sliders.count
        
        for i in 0..<viewCount{
            let slider = self.sliders[i]
            let label = self.labels[i]
            let textField = self.textFields[i]
            
            slider.transform = CGAffineTransform(rotationAngle: CGFloat(-M_PI_2))
            slider.frame = CGRect(x: CGFloat(i)*viewWidth, y: 20, width: viewWidth, height: viewHeight-80)
            label.frame  = CGRect(x: CGFloat(i)*viewWidth, y: 0, width: viewWidth, height: 20)
            
            textField.frame  = CGRect(x: CGFloat(i)*viewWidth+viewWidth*0.16, y: viewHeight-60, width: viewWidth*0.68, height: 40)
        }
    }
    
    func sliderValueDidChanged(sender:UISlider!){
        AMPlayer.shareInstance.setEqualizerGain(tag: sender.tag, gain: sender.value)
    }
    
    func textFieldValueDidEnd(sender:UITextField!){
        let value = Float(sender.text!)
        
        var gain = value == nil ? self.gains[tag] : value!
        if gain < -12 { gain = -12 }
        if gain > 12 { gain = 12 }
        
        sender.text = String(gain)
        
        if value == nil { return }
        
        UIView.animate(withDuration: 0.3, animations: {
            self.sliders[sender.tag].setValue(gain, animated: true)
        })
        
        self.gains[sender.tag] = gain
        
        AMPlayer.shareInstance.setEqualizerGain(tag: sender.tag, gain: gain)
    }
    
    func updateView(){
        let values = AMPlayer.shareInstance.equalizerGain
        
        UIView.animate(withDuration: 0.3, animations: {
            for i in 0..<values.count {
                self.sliders[i].setValue(values[i],animated: true)
            }
        })
    }
}
