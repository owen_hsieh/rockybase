//
//  AncGainViewController.swift
//  rockybase
//
//  Created by owen on 2016/11/1.
//  Copyright © 2016年 Ampacs. All rights reserved.
//

import UIKit

class AncGainViewController : UIViewController, UITextFieldDelegate {
    /* IBOutlets */
    @IBOutlet weak var fflCurrentValueLabel:UILabel!
    @IBOutlet weak var ffrCurrentValueLabel:UILabel!
    @IBOutlet weak var fblCurrentValueLabel:UILabel!
    @IBOutlet weak var fbrCurrentValueLabel:UILabel!
    
    @IBOutlet weak var ancStateSwitch:UISwitch!
    @IBOutlet weak var fflTestSetAEdit:AncUITextField!
    @IBOutlet weak var ffrTestSetAEdit:AncUITextField!
    @IBOutlet weak var fblTestSetAEdit:AncUITextField!
    @IBOutlet weak var fbrTestSetAEdit:AncUITextField!
    
    @IBOutlet weak var fflTestSetBEdit:AncUITextField!
    @IBOutlet weak var ffrTestSetBEdit:AncUITextField!
    @IBOutlet weak var fblTestSetBEdit:AncUITextField!
    @IBOutlet weak var fbrTestSetBEdit:AncUITextField!
    @IBOutlet weak var refreshButton:UIButton!
    @IBOutlet weak var testSetAButton:UIButton!
    @IBOutlet weak var testSetBButton:UIButton!
    
    
    /* UIViewController overrides */
    override func viewDidLoad(){
        super.viewDidLoad()
        
        self.fflTestSetAEdit.delegate = self
        self.ffrTestSetAEdit.delegate = self
        self.fblTestSetAEdit.delegate = self
        self.fbrTestSetAEdit.delegate = self
        
        self.fflTestSetBEdit.delegate = self
        self.ffrTestSetBEdit.delegate = self
        self.fblTestSetBEdit.delegate = self
        self.fbrTestSetBEdit.delegate = self
    }
    
    override func viewDidAppear(_ animated:Bool){
        super.viewDidAppear(animated)
    }
    
    
    override func viewDidDisappear(_ animated:Bool){
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning(){
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    /* IBActions */
    @IBAction func didRefreshTouchUpInside(_ sender:Any){
        
    }
    @IBAction func didRefreshTouchUpOutside(_ sender:Any){
        
    }
    @IBAction func didChangeAncState(_ sender:Any){
        
    }
    @IBAction func didTestSetATouchUpInside(_ sender:Any){
        
    }
    @IBAction func didTestSetATouchUpOutside(_ sender:Any){
        
    }
    @IBAction func didTestSetBTouchUpInside(_ sender:Any){
        
    }
    @IBAction func didTestSetBTouchUpOutside(_ sender:Any){
        
    }
    @IBAction func didWriteSettingsTouchUpInside(_ sender:Any){
        
    }
    @IBAction func didWriteSettingsTouchUpOutside(_ sender:Any){
        
    }
}

