//
//  AMPlayer.swift
//  AmpacsMusic
//
//  Created by owen on 2016/8/31.
//  Copyright © 2016年 Ampacs. All rights reserved.
//

import MediaPlayer

class AMPlayerDelegate : NSObject {
    
    var updateCurrentTime:AMUpdateCurrentTimeCallback
    var onStart:AMStartCallback
    var onFinish:AMFinishCallback
    
    override init(){
        updateCurrentTime = {_ in }
        onStart = {}
        onFinish = {}
    }
}

class AMPlayer {
    enum LoopMode {
        case noLoop, loopAll, loopOne
    }
    
    static let shareInstance:AMPlayer = AMPlayer()
    fileprivate let eqFrequency:[Float] = [32.0 , 64.0, 125.0, 250.0, 500.0, 1000.0, 2000.0, 4000.0, 8000.0, 16000.0]
    fileprivate var mixerHost:AMMixerHost
    fileprivate var analyzer:AMAnalyzer
    
    fileprivate var delegates:[AMPlayerDelegate] = []
    
    fileprivate var _trackIndex:Int = -1
    
    fileprivate var _percentage:Float = 0
    var percentage:Float{
        get{
            return _percentage
        }
        set(value){
            self._percentage = value
            self.mixerHost.setPercentage(self._percentage)
        }
    }
    
    var track:MPMediaItemCollection
    
    var equalizerPresetIndex:Int = 0{
        didSet{
            self.equalizerPresetIndex = self.mixerHost.setEQPreset(self.equalizerPresetIndex)
        }
    }
    
    init(){
        self.track = MPMediaItemCollection(items:[])
        self.mixerHost = AMMixerHost(frequency: self.eqFrequency)
        self.analyzer = AMAnalyzer.shareInstance
        
        var mixerDelegate:AMMixerHostDelegate = AMMixerHostDelegate()
        
        mixerDelegate = AMMixerHostDelegate()
        mixerDelegate.updateCurrentTime = {
            (currentTime)in
            self._percentage = self.mixerHost.getPercentage()
            
            for __delegate in self.delegates {
                __delegate.updateCurrentTime(currentTime)
            }
        }
        mixerDelegate.onStart = {
            for __delegate in self.delegates {
                __delegate.onStart()
            }
            
        }
        mixerDelegate.onFinish = {
            if self.loopMode == LoopMode.loopOne {
                self.percentage = 0
            } else {
                self.next()
            }
            for __delegate in self.delegates {
                __delegate.onFinish()
            }
            
        }
        mixerDelegate.handleSample = {
            (sampleBuffer:SampleBuffer) in
            self.analyzer.executeFFT(sampleBuffer)
        }
        
        self.mixerHost.setDelegate(mixerDelegate)
    }
    
    deinit {
        
    }
    
    var trackIndex:Int {
        get{ return self._trackIndex }
        set(trackIndex) {
            self._trackIndex = trackIndex
            self._trackIndex = self._trackIndex < 0 ? 0 : self._trackIndex
            self._trackIndex = self._trackIndex >= self.track.count ? 0 : self._trackIndex
            if self.track.count == 0 { return }
            
            let mpItem = track.items[self._trackIndex]
            
            if mpItem.assetURL != self.mixerHost.currentMediaItem?.assetURL {
                self.analyzer.resetLevels()
                self.percentage = 0
                self.mixerHost.currentMediaItem = track.items[self._trackIndex]
            }
        }
    }
    
    var mediaItem:MPMediaItem? {
        get{
            if self._trackIndex < 0 || self._trackIndex >= self.track.count { return nil }
            return self.track.items[self._trackIndex]
        }
    }
    
    var isPlaying:Bool {
        get{
            return self.mixerHost.isPlaying
        }
    }
    
    var levels:[Float]{
        get{
            return self.analyzer.levelOverall
        }
    }
    
    var equalizerGain:[Float]{
        get{
            return self.mixerHost.getFrequencyGain()
        }
    }
    
    var shuffle:Bool = false
    var loopMode:LoopMode = LoopMode.noLoop
    
    func addDelegate(_ delegate:AMPlayerDelegate){
        for idx in 0..<self.delegates.count {
            if self.delegates[idx] == delegate {
                return
            }
        }
        
        self.delegates.append(delegate)
    }
    
    func removeDelegate(_ delegate:AMPlayerDelegate){
        for idx in 0..<self.delegates.count {
            if self.delegates[idx] == delegate {
                self.delegates.remove(at: idx)
                return
            }
        }
    }
    
    func resume(_ forced:Bool = false){
        self.mixerHost.start(forced)
    }
    
    func pause(){
        self.mixerHost.stop()
    }
    
    func toggleResume(){
        if self.isPlaying {
            self.pause()
        } else {
            self.resume()
        }
    }
    
    func stop(){
        self.mixerHost.stop()
    }
    
    func prev() {
        if self.trackIndex == 0 {
            self.mixerHost.setTime(0)
            return
        }
        
        self.trackIndex = self.trackIndex-1
    }
    
    func next(){
        if self.trackIndex == self.track.count-1 {
            if self.loopMode == LoopMode.loopAll {
                self.trackIndex = 0
            } else {
                self.mixerHost.stop()
                self.mixerHost.setTime(0)
                return
            }
        } else {
            self.trackIndex = self.trackIndex+1
        }
    }
    
    func toggleShuffle(){
        self.shuffle = !self.shuffle
    }
    
    func toggleLoopMode(){
        if self.loopMode == LoopMode.noLoop {
            self.loopMode = LoopMode.loopAll
        } else if self.loopMode == LoopMode.loopAll {
            self.loopMode = LoopMode.loopOne
        } else {
            self.loopMode = LoopMode.noLoop
        }
    }
    
    func frequencyBand() -> [Float] {
        return self.eqFrequency
    }
    
    func frequencyGain() -> [Float] {
        return self.mixerHost.getFrequencyGain()
    }
    
    func setEqualizerGain(tag:Int, gain:Float){
        self.mixerHost.setEQGain(frequencyTag: tag, gainValue: gain)
    }
    
    func setEqualizerGlobalGain(gain:Float){
        self.mixerHost.setEQGlobalGain(gain)
    }
    
    func resetEqualizer(){
        self.mixerHost.resetEQ()
    }
    
    func equalizerPresets() -> [String] {
        return self.mixerHost.getIpodPresets()
    }
    
}
