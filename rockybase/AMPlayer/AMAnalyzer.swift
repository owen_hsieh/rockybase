//
//  AMAnalyzer.swift
//  AmpacsMusic
//
//  Created by owen on 2016/9/2.
//  Copyright © 2016年 Ampacs. All rights reserved.
//
//  Reference Project:
//      https://github.com/douban/DOUAudioStreamer


import Foundation
import Accelerate

class AMAnalyzer {
    static let shareInstance:AMAnalyzer = AMAnalyzer()
    
    class var kLevelLength:Int{
        get { return 128 }
    }
    fileprivate var fftScale:Float = 0.5
    fileprivate let fftLog2n:vDSP_Length = UInt(round(log2(Float(AMAnalyzer.kLevelLength))))
    fileprivate let fftLength:vDSP_Length = UInt(AMAnalyzer.kLevelLength / 2)
    fileprivate var fftNormalizeSize:Int32 = Int32(AMAnalyzer.kLevelLength / 4)
    fileprivate var fftMultiplier:Float = 1/16
    fileprivate var fftIncrement:Float = sqrtf(1/16) //equal fftMultiplier
    
    fileprivate let fft:FFTSetup
    
    fileprivate var hammWindow:UnsafeMutablePointer<Float>
    
    fileprivate var complexSplit:COMPLEX_SPLIT
    
    fileprivate var levelDataLeft:UnsafeMutablePointer<Float>
    fileprivate var levelDataRight:UnsafeMutablePointer<Float>
    fileprivate var levelDataOverAll:UnsafeMutablePointer<Float>
    
    fileprivate init() {
        self.complexSplit = COMPLEX_SPLIT(realp: UnsafeMutablePointer<Float>.allocate(capacity: AMAnalyzer.kLevelLength/2), imagp: UnsafeMutablePointer<Float>.allocate(capacity: AMAnalyzer.kLevelLength/2))

        self.levelDataLeft = UnsafeMutablePointer<Float>.allocate(capacity: AMAnalyzer.kLevelLength)
        self.levelDataRight = UnsafeMutablePointer<Float>.allocate(capacity: AMAnalyzer.kLevelLength)
        self.levelDataOverAll = UnsafeMutablePointer<Float>.allocate(capacity: AMAnalyzer.kLevelLength)
        self.hammWindow = UnsafeMutablePointer<Float>.allocate(capacity: AMAnalyzer.kLevelLength)
        
        memset(self.levelDataLeft, 0, AMAnalyzer.kLevelLength)
        memset(self.levelDataRight, 0, AMAnalyzer.kLevelLength)
        
        vDSP_hamm_window(self.hammWindow, UInt(AMAnalyzer.kLevelLength), 0)
        
        self.fft = vDSP_create_fftsetup(self.fftLog2n, Int32(kFFTRadix2))!
    }
    
    deinit{
        free(self.hammWindow)
        free(self.levelDataLeft)
        free(self.levelDataRight)
        free(self.levelDataOverAll)
        
        free(self.complexSplit.realp)
        free(self.complexSplit.imagp)
        
        vDSP_destroy_fftsetup(self.fft)
    }
    
    var levelsLeft:Array<Float>{
        get {
            return Array<Float>(UnsafeBufferPointer(start: self.levelDataLeft, count: AMAnalyzer.kLevelLength))
        }
    }
    
    var levelsRight:Array<Float>{
        get {
            return Array<Float>(UnsafeBufferPointer(start: self.levelDataRight, count: AMAnalyzer.kLevelLength))
        }
    }
    
    var levelOverall:Array<Float>{
        get {
            return Array<Float>(UnsafeBufferPointer(start: self.levelDataOverAll, count: AMAnalyzer.kLevelLength))
        }
    }
    
    func executeFFT(_ buffer:SampleBuffer){
        if buffer.sampleSize <= 0 { return }
        
        self.process(UnsafeMutablePointer<Float>(mutating: buffer.audioDataLeft), levels: self.levelDataLeft)
        if buffer.isStereo {
            self.process(UnsafeMutablePointer<Float>(mutating: buffer.audioDataRight), levels: self.levelDataRight)
        }
        
        self.updateLevels()
    }
    
    func resetLevels(){
        vDSP_vclr(self.levelDataLeft, 1, UInt(AMAnalyzer.kLevelLength))
        vDSP_vclr(self.levelDataOverAll, 1, UInt(AMAnalyzer.kLevelLength))
        vDSP_vclr(self.levelDataOverAll, 1, UInt(AMAnalyzer.kLevelLength))
    }
    
    /* private fields */
    
    fileprivate func process(_ vectors:UnsafeMutablePointer<Float>,
                         levels:UnsafeMutablePointer<Float>){
        self.splitInterleavedComplexVectors(vectors)
        self.forwardFFT(vectors)
        self.normalizeToLevels(vectors, levels:levels)
    }
    
    fileprivate func updateLevels(){
        vDSP_vadd(self.levelDataLeft, 1, self.levelDataRight, 1, self.levelDataOverAll, 1, UInt(AMAnalyzer.kLevelLength))
        vDSP_vsmul(self.levelDataOverAll, 1, &self.fftScale, self.levelDataOverAll, 1, UInt(AMAnalyzer.kLevelLength))
        
        var min:Float = 0
        var max:Float = 1
        vDSP_vclip(self.levelDataOverAll, 1, &min, &max, self.levelDataOverAll, 1, UInt(AMAnalyzer.kLevelLength))
    }
    
    fileprivate func splitInterleavedComplexVectors(_ vectors:UnsafeMutablePointer<Float>){
        vDSP_vmul(vectors, 1, self.hammWindow, 1, vectors, 1, UInt(AMAnalyzer.kLevelLength))
        let raw = UnsafeRawPointer(vectors)
        let complexPtr = raw.assumingMemoryBound(to: COMPLEX.self)
        vDSP_ctoz(complexPtr, 2, &self.complexSplit, 1, self.fftLength)
    }
    
    fileprivate func forwardFFT(_ vectors:UnsafeMutablePointer<Float>){
        vDSP_fft_zrip(self.fft, &self.complexSplit, 1, self.fftLog2n, Int32(kFFTDirection_Forward))
        vDSP_zvabs(&self.complexSplit, 1, vectors, 1, self.fftLength)
        
        vDSP_vsmul(vectors, 1, &self.fftScale, vectors, 1, self.fftLength)
    }
    
    fileprivate func normalizeToLevels(_ vectors:UnsafeMutablePointer<Float>,
                                   levels:UnsafeMutablePointer<Float>){
        vDSP_vsq(vectors, 1, vectors, 1, UInt(self.fftNormalizeSize))
        vvlog10f(vectors, vectors, &self.fftNormalizeSize)
        
        vDSP_vsmsa(vectors, 1, &self.fftMultiplier, &self.fftIncrement, vectors, 1, self.fftLength)
        
        let selector:Float = (Float)(self.fftNormalizeSize-1) / (Float)(AMAnalyzer.kLevelLength)
        
        for i in 0..<AMAnalyzer.kLevelLength{
            levels[i] = vectors[Int(1 + selector*(Float)(i))]
        }
    }
}
