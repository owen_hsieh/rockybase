//
//  AMMixerHost.swift
//  AmpacsMusic
//
//  Created by owen on 2016/8/25.
//  Copyright © 2016年 Ampacs. All rights reserved.
//

import AVFoundation
import AudioToolbox
import Accelerate
import MediaPlayer

public typealias AMUpdateCurrentTimeCallback = (Float)->Void
public typealias AMStartCallback = ()->Void
public typealias AMFinishCallback = ()->Void
public typealias AMHandleSampleCallback = (_ buffer:SampleBuffer)->Void

open class AMMixerHostDelegate {
    var updateCurrentTime:AMUpdateCurrentTimeCallback
    var onStart:AMStartCallback
    var onFinish:AMFinishCallback
    var handleSample:AMHandleSampleCallback
    
    init(){
        updateCurrentTime = {_ in }
        onStart = {}
        onFinish = {}
        handleSample = {_ in}
    }
}

public struct SoundStruct {
    var isStereo:Bool
    var totalFrame:Int64
    var currentFrame:UInt32
    
    var dataLeft:UnsafeMutablePointer<Float>? = nil
    var dataRight:UnsafeMutablePointer<Float>? = nil
}

public struct SampleBuffer {
    var sampleSize:Int
    var audioDataLeft:Array<Float>
    var audioDataRight:Array<Float>
    var isStereo:Bool
}

class AMSegment {
    var start:Int64
    var end:Int64
    var pos:Int64
    
    init(){
        self.start = 0
        self.end = 0
        self.pos = 0
    }
    
    init( start:Int64, end:Int64){
        self.start = start
        self.end = end
        self.pos = self.start
    }
    
    func contains(_ pos:Int64) -> Bool {
        return (pos < self.start || pos > self.end)
    }
    
    func containsAtFront(_ pos:Int64)-> Bool {
        return (self.start <= pos && pos <= self.pos)
    }
    
    func containsAtBack(_ pos:Int64)-> Bool {
        return (self.pos <= pos && pos <= self.end)
    }
    
    func assign(_ segment:AMSegment){
        self.start = segment.start
        self.end = segment.end
        self.pos = segment.pos
    }
    
    func toString()->String{
        return String.init(format: "{ start:%d, end:%d }", start, end)
    }
}

class AMSegmentQueue {
    fileprivate var segments:[AMSegment]
    
    init() {
        segments = []
    }
    
    var isEmpty:Bool {
        get{
            return segments.count == 0
        }
    }
    
    func removeAll(){
        segments.removeAll()
    }
    
    func enqueue(segment:AMSegment){
        segments.append(segment)
    }
    
    func dequeue()->AMSegment?{
        if self.isEmpty { return nil }
        return segments.remove(at: 0)
    }
    
    func hasSegmentContains(_ pos:Int64)->Bool{
        return (self.getSegmentContains(pos) != nil)
    }
    
    func getSegmentContains(_ pos:Int64)->AMSegment?{
        if self.isEmpty { return nil }
        
        for segment in segments {
            if segment.contains(pos) {
                return segment
            }
        }
        
        return nil
    }
    
    func getSegmentAt(_ index:Int)->AMSegment?{
        if index < 0 || index > (segments.count-1) { return nil }
        return segments[index]
    }
    
    func getSegmentIndexContains(_ pos:Int64)->Int{
        if self.isEmpty { return -1 }
        
        for idx in 0..<segments.count{
            if segments[idx].contains(pos){
                return idx
            }
        }
        
        return -1
    }
    
    func updateSegment(at index:Int,with segment:AMSegment){
        if index < 0 || index > (segments.count-1) { return }
        segments[index].assign(segment)
    }
    
    
}

open class AMMixerHost {
    /* private fields */
    
    fileprivate var audioSession:AVAudioSession!
    
    //file url
    fileprivate var assetURL:URL!
    
    // frequencies for band equalizer
    fileprivate var eqFrequencyBand:[Float]
    fileprivate var eqFrequencyGain:[Float]
    fileprivate var eqGlobalGain:Float
    
    // the main audio graph
    fileprivate var augraph:AUGraph?
    // some of the audio units in this app
    fileprivate var mixerUnit:AudioUnit?    // mixer audio unit
    fileprivate var ipodUnit:AudioUnit?     // ipod audio unit
    fileprivate var eqUnit:AudioUnit?       // master effect on mixer output
    fileprivate var ioUnit:AudioUnit?       // remote io unit (speaker)
    
    // audio graph nodes
    fileprivate var mixerNode:AUNode        // multichannel mixer node
    fileprivate var ipodNode:AUNode         // ipod preset node
    fileprivate var eqNode:AUNode           // master mix effect node
    fileprivate var ioNode:AUNode           // io unit node
    
    fileprivate var factoryPreset:UnsafeMutablePointer<CFArray>?
    fileprivate var factoryPresetCount:Int = 0
    
    // media item
    fileprivate var mediaItem:MPMediaItem!
    
    // audio graph sample rate
    fileprivate var graphSampleRate:Float64 = 0
    
    fileprivate var isLoading:Bool
    
    //audio time
    fileprivate var duration:Float = 0
    
    //audio segments
    fileprivate var amSegment:AMSegment = AMSegment()
    fileprivate var amSegmentQueue:AMSegmentQueue = AMSegmentQueue()
    fileprivate var bufferSeg:UnsafeMutableAudioBufferListPointer!
    fileprivate var leftSeg:UnsafeMutablePointer<Float>?
    fileprivate var rightSeg:UnsafeMutablePointer<Float>?
    fileprivate var segFileRef:ExtAudioFileRef?
    fileprivate var segmentQueue:DispatchQueue?
    fileprivate var fileMutex = Mutex()
    fileprivate var renderMutex = Mutex()
    fileprivate var executeReadSeg = {}
    
    fileprivate var stereoStreamDescription:AudioStreamBasicDescription
    fileprivate var monoStreamDescription:AudioStreamBasicDescription
    // eq unit description
    fileprivate var eqStreamDescription:AudioStreamBasicDescription
    
    fileprivate var audioStruct:SoundStruct
    
    fileprivate var delegate:AMMixerHostDelegate?
    
    init(frequency:[Float]){
        self.isLoading = false
        
        self.augraph = nil
        
        self.mixerNode = AUNode()
        self.ipodNode = AUNode()
        self.eqNode = AUNode()
        self.ioNode = AUNode()
        
        self.ioUnit = nil
        self.mixerUnit = nil
        self.eqUnit = nil
        self.ipodUnit = nil
        
        self.stereoStreamDescription = AudioStreamBasicDescription()
        self.monoStreamDescription = AudioStreamBasicDescription()
        self.eqStreamDescription = AudioStreamBasicDescription()
        
        self.eqFrequencyBand = Array(frequency)
        self.eqFrequencyGain = Array(repeating: 0, count: frequency.count)
        self.eqGlobalGain = 0
        
        self.audioStruct = SoundStruct(isStereo: false, totalFrame: 0, currentFrame: 0, dataLeft: nil, dataRight: nil)
        
        self.setupAudioSession()
        self.setupStreamDescription()
        self.setupAUGraph()
        self.setupAudioUnit()
        self.setupConnectAUNodes()
        self.setupFinisher()
    }
    
    deinit {
        DisposeAUGraph(self.augraph!)
        
        if self.factoryPreset != nil {
            free(self.factoryPreset)
        }
        
        let fileLengthFrames = self.audioStruct.totalFrame
        if self.audioStruct.dataLeft != nil {
            self.audioStruct.dataLeft?.deallocate(capacity: Int(fileLengthFrames))
            self.audioStruct.dataLeft = nil
        }
        if self.audioStruct.dataRight != nil {
            self.audioStruct.dataRight?.deallocate(capacity: Int(fileLengthFrames))
            self.audioStruct.dataRight = nil
        }
    }
    
    fileprivate func setupAudioSession() {
        // setup the session
        self.audioSession = AVAudioSession.sharedInstance()
        
        do {
            try self.audioSession.setCategory(AVAudioSessionCategoryPlayback)
        } catch {
            debugError(error)
        }
        
        self.graphSampleRate = 44100.0    // Hertz
        
        do {
            try self.audioSession.setPreferredSampleRate(self.graphSampleRate)
        }catch{
            debugError(error)
        }
        
        let currentBufferDuration =  (TimeInterval) (1024.0 / self.graphSampleRate)
        
        do {
            try self.audioSession.setPreferredIOBufferDuration(currentBufferDuration)
        } catch {
            debugError(error)
        }
        
        do {
            try self.audioSession.setActive(true)
        } catch {
            debugError(error)
        }
        
        self.graphSampleRate = self.audioSession.sampleRate
    }
    
    fileprivate func setupStreamDescription(){
        let bytesPerSample:UInt32 = UInt32(MemoryLayout<Float>.size)
        
        /* stereo */
        stereoStreamDescription.mFormatID          = kAudioFormatLinearPCM
        stereoStreamDescription.mFormatFlags       = kAudioFormatFlagIsFloat | kAudioFormatFlagsNativeEndian | kAudioFormatFlagIsPacked | kAudioFormatFlagIsNonInterleaved
        stereoStreamDescription.mBytesPerPacket    = bytesPerSample
        stereoStreamDescription.mFramesPerPacket   = 1
        stereoStreamDescription.mBytesPerFrame     = bytesPerSample
        stereoStreamDescription.mChannelsPerFrame  = 2                   // 2 indicates stereo
        stereoStreamDescription.mBitsPerChannel    = 8 * bytesPerSample
        stereoStreamDescription.mSampleRate        = self.graphSampleRate
        
        /* mono */
        monoStreamDescription.mFormatID          = kAudioFormatLinearPCM
        monoStreamDescription.mFormatFlags       = kAudioFormatFlagIsFloat | kAudioFormatFlagsNativeEndian | kAudioFormatFlagIsPacked | kAudioFormatFlagIsNonInterleaved
        monoStreamDescription.mBytesPerPacket    = bytesPerSample
        monoStreamDescription.mFramesPerPacket   = 1
        monoStreamDescription.mBytesPerFrame     = bytesPerSample
        monoStreamDescription.mChannelsPerFrame  = 1;                    // 1 indicates mono
        monoStreamDescription.mBitsPerChannel    = 8 * bytesPerSample
        monoStreamDescription.mSampleRate        = self.graphSampleRate
    }
    
    fileprivate func setupAUGraph(){
        var result:OSStatus = noErr
        // Create a new audio processing graph.
        result = NewAUGraph(&(self.augraph))
        
        if noErr != result {
            debugError("NewAUGraph", status:result)
            return
        }
        
        // Multichannel mixer unit
        var mixerUnitDescription = AudioComponentDescription(
            componentType:kAudioUnitType_Mixer,
            componentSubType:kAudioUnitSubType_MultiChannelMixer,
            componentManufacturer:kAudioUnitManufacturer_Apple,
            componentFlags:0,
            componentFlagsMask:0)
        
        // ipod unit for factory preset
        var ipodUnitDescription = AudioComponentDescription(
            componentType:kAudioUnitType_Effect,
            componentSubType:kAudioUnitSubType_AUiPodEQ,
            componentManufacturer:kAudioUnitManufacturer_Apple,
            componentFlags:0,
            componentFlagsMask:0)
        
        // au unit effect for mixer output - NBandEQ
        var eqUnitDescription = AudioComponentDescription(
            componentType:kAudioUnitType_Effect,
            componentSubType:kAudioUnitSubType_NBandEQ,
            componentManufacturer:kAudioUnitManufacturer_Apple,
            componentFlags:0,
            componentFlagsMask:0)
        
        
        // final io
        var ioUnitDescription = AudioComponentDescription(
            componentType:kAudioUnitType_Output,
            componentSubType:kAudioUnitSubType_RemoteIO,
            componentManufacturer:kAudioUnitManufacturer_Apple,
            componentFlags:0,
            componentFlagsMask:0)
        
        // Add the nodes to the audio processing graph
        // mixer unit
        result = AUGraphAddNode(self.augraph!, &mixerUnitDescription, &mixerNode)
        
        if noErr != result {
            debugError("AUGraphNewNode/mixerNode", status:result)
            return
        }
        
        // au effect unit
        result = AUGraphAddNode(self.augraph!, &eqUnitDescription, &eqNode)
        
        if noErr != result {
            debugError("AUGraphNewNode/eqNode", status:result)
            return
        }
        
        result = AUGraphAddNode(self.augraph!, &ipodUnitDescription, &ipodNode)
        
        if noErr != result {
            debugError("AUGraphNewNode/ipodNode", status:result)
            return
        }
        
        // io unit
        result = AUGraphAddNode(self.augraph!, &ioUnitDescription, &ioNode)
        
        if noErr != result {
            debugError("AUGraphNewNode/ioNode", status:result)
            return
        }
        
        result = AUGraphOpen (self.augraph!)
        
        if noErr != result {
            debugError("AUGraphOpen", status:result)
            return
        }
        
    }
    
    fileprivate func setupAudioUnit() {
        self._setup_eqUnit()
        self._setup_ipodUnit()
        self._setup_mixerUnit()
        self._setup_ioUnit()
    }
    
    fileprivate func _setup_eqUnit() {
        var result:OSStatus = noErr
        
        result = AUGraphNodeInfo (self.augraph!,eqNode,nil,&eqUnit)
        
        if noErr != result {
            debugError("AUGraphNodeInfo/eqUnit", status:result)
            return
        }
        
        /* setup equalizer unit*/
        var descriptionSize:UInt32 = UInt32(MemoryLayout<AudioStreamBasicDescription>.size)
        memset(&eqStreamDescription, 0, MemoryLayout<AudioStreamBasicDescription>.size)
        
        result = AudioUnitGetProperty(
            eqUnit!,
            kAudioUnitProperty_StreamFormat,
            kAudioUnitScope_Input,
            0,
            &eqStreamDescription,
            &descriptionSize)
        
        if noErr != result {
            debugError("AudioUnitGetProperty/eqUnit/StreamFormat/Input", status:result)
            return
        }
        
        // make sure the sample rate is correct
        eqStreamDescription.mSampleRate = self.graphSampleRate
        
        result = AudioUnitSetProperty(
            eqUnit!,
            kAudioUnitProperty_StreamFormat,
            kAudioUnitScope_Input,
            0,
            &eqStreamDescription,
            UInt32(MemoryLayout<AudioStreamBasicDescription>.size))
        
        if noErr != result {
            debugError("AudioUnitSetProperty/eqUnit/StreamFormat/Input", status:result)
            return
        }
        
        result = AudioUnitSetProperty (
            eqUnit!,
            kAudioUnitProperty_SampleRate,
            kAudioUnitScope_Output,
            0,
            &(self.graphSampleRate),
            UInt32(MemoryLayout<Float64>.size)
        )
        
        if noErr != result {
            debugError("AudioUnitSetProperty/SampleRate/Output", status:result)
            return
        }
        
        // Frequency bands
        var noBands:UInt32 = UInt32(eqFrequencyBand.count)
        
        // Set the number of bands first
        result = AudioUnitSetProperty(
            eqUnit!,
            kAUNBandEQProperty_NumberOfBands,
            kAudioUnitScope_Global,
            0,
            &noBands,
            UInt32(MemoryLayout<UInt32>.size))
        
        if noErr != result {
            debugError("AudioUnitSetProperty/NumberOfBands/Global", status:result)
            return
        }
        
        // Set band frequency
        for i in 0..<self.eqFrequencyBand.count {
            let val = self.eqFrequencyBand[i]
            result = AudioUnitSetParameter(
                eqUnit!,
                kAUNBandEQParam_Frequency + UInt32(i),
                kAudioUnitScope_Global,
                0,
                (AudioUnitParameterValue)(val),
                0)
            
            if noErr != result {
                debugError("AudioUnitSetParameter/Frequency/Global", status:result)
                return
            }
        }
        
        for i in 0..<self.eqFrequencyBand.count {
            result = AudioUnitSetParameter(
                eqUnit!,
                kAUNBandEQParam_BypassBand + UInt32(i),
                kAudioUnitScope_Global,
                0,
                (AudioUnitParameterValue)(0),
                0)
            self.eqFrequencyGain[i] = 0
            if noErr != result {
                debugError("AudioUnitSetParameter/BypassBand/Global", status:result)
                return
            }
        }
    }
    
    fileprivate func _setup_ipodUnit() {
        var result:OSStatus = noErr
        
        result = AUGraphNodeInfo (self.augraph!,ipodNode,nil,&ipodUnit)
        
        if noErr != result {
            debugError("AUGraphNodeInfo/ipodUnit", status:result)
            return
        }
        
        result = AudioUnitSetProperty(
            ipodUnit!,
            kAudioUnitProperty_StreamFormat,
            kAudioUnitScope_Input,
            0,
            &eqStreamDescription,
            UInt32(MemoryLayout<AudioStreamBasicDescription>.size))
        
        if noErr != result {
            debugError("AudioUnitSetProperty/ipodUnit/StreamFormat/Input", status:result)
            return
        }
        
        
        result = AudioUnitSetProperty (
            ipodUnit!,
            kAudioUnitProperty_SampleRate,
            kAudioUnitScope_Output,
            0,
            &(self.graphSampleRate),
            UInt32(MemoryLayout<Float64>.size)
        )
        
        if noErr != result {
            debugError("AudioUnitSetProperty/ipodUnit/SampleRate/Output", status:result)
            return
        }
        
        
        let outData:UnsafeMutableRawPointer = UnsafeMutableRawPointer.allocate(bytes: MemoryLayout<CFArray>.size, alignedTo: 0)
        var ioDataSize:UInt32 = UInt32(MemoryLayout<CFArray>.size)
        
        result = AudioUnitGetProperty(ipodUnit!, kAudioUnitProperty_FactoryPresets, kAudioUnitScope_Global, 0, outData, &ioDataSize)
        
        if noErr != result {
            debugError("AudioUnitGetProperty/ipodUnit/FactoryPresets/Global", status: result)
            return
        }
        
        self.factoryPreset = outData.assumingMemoryBound(to: CFArray.self)
        self.factoryPresetCount = CFArrayGetCount(self.factoryPreset?.pointee)
    }
    
    fileprivate func _setup_mixerUnit(){
        var result = AUGraphNodeInfo (self.augraph!,mixerNode,nil,&mixerUnit)
        
        if noErr != result {
            debugError("AUGraphNodeInfo/mixerUnit", status:result)
            return
        }
        
        // Mixer unit Setup
        var busCount:UInt32 = 1 // bus count for mixer unit input
        let mainBus:UInt32  = 0
        
        result = AudioUnitSetProperty(
            mixerUnit!,
            kAudioUnitProperty_ElementCount,
            kAudioUnitScope_Input,
            0,
            &busCount,
            UInt32(MemoryLayout<UInt32>.size)
        )
        
        if noErr != result {
            debugError("AudioUnitSetProperty/mixerUnit/ElementCount/Input", status:result)
            return
        }
        
        var maximumFramesPerSlice:UInt32 = 4096
        
        result = AudioUnitSetProperty (
            mixerUnit!,
            kAudioUnitProperty_MaximumFramesPerSlice,
            kAudioUnitScope_Global,
            0,
            &maximumFramesPerSlice,
            UInt32(MemoryLayout<UInt32>.size)
        )
        
        if noErr != result {
            debugError("AudioUnitSetProperty/mixerUnit/maximumFramesPerSlice", status:result)
            return
        }
        
        /* using render callback to play music */
        var callbackStruct:AURenderCallbackStruct =
            AURenderCallbackStruct(inputProc: self.auRenderCallback,
                                   inputProcRefCon: unsafeBitCast(self, to: UnsafeMutableRawPointer.self))
        
        result = AUGraphSetNodeInputCallback (
            self.augraph!,
            mixerNode,
            0,
            &callbackStruct
        )
        
        if noErr != result {
            debugError("AUGraphSetNodeInputCallback/mixerNode", status:result)
            return
        }
        
        result = AudioUnitSetProperty (
            mixerUnit!,
            kAudioUnitProperty_StreamFormat,
            kAudioUnitScope_Input,
            mainBus,
            &stereoStreamDescription,
            UInt32(MemoryLayout<AudioStreamBasicDescription>.size)
        )
        
        if noErr != result {
            debugError("AudioUnitSetProperty/mixerUnit/StreamFormat/Input", status:result)
            return
        }
        
        result = AudioUnitSetProperty(
            mixerUnit!,
            kAudioUnitProperty_StreamFormat,
            kAudioUnitScope_Output,
            0,
            &eqStreamDescription,
            UInt32(MemoryLayout<AudioStreamBasicDescription>.size))
        
        
        if noErr != result {
            debugError("AudioUnitSetProperty/mixerUnit/StreamFormat/Output", status:result)
            return
        }
    }
    
    fileprivate func _setup_ioUnit(){
        let result = AUGraphNodeInfo(self.augraph!,ioNode,nil,&ioUnit)
        
        if noErr != result {
            debugError("AUGraphNodeInfo - i/o:", status:result)
            return
        }
    }
    
    fileprivate func setupConnectAUNodes(){
        var result:OSStatus = noErr
        
        let auNodes:[AUNode] = [mixerNode, ipodNode, eqNode, ioNode]
        let auNodeNames:[String] = ["mixer", "ipod", "eq", "io"]
        
        for idx in 0..<auNodes.count-1 {
            let src = auNodes[idx]
            let dst = auNodes[idx+1]
            result = AUGraphConnectNodeInput (
                self.augraph!,
                src,    // source node
                0,      // source node output bus number
                dst,    // destination node
                0       // desintation node input bus number
            )
            
            if noErr != result {
                debugError(auNodeNames[idx]+" to "+auNodeNames[idx+1]+" err AUGraphConnectNodeInput:", status:result)
                return
            }
        }
    }
    
    fileprivate func setupFinisher(){
        let result = AUGraphInitialize (self.augraph!)
        
        if noErr != result {
            debugError("AUGraphInitialize", status:result)
            return
        }
    }
    
    fileprivate func readSegments(){
        DispatchQueue.global().async(execute: {
            self.fileMutex.lock()
            
            // Instantiate an extended audio file object.
            
            if self.segFileRef != nil {
                self.amSegmentQueue.removeAll()
                ExtAudioFileDispose(self.segFileRef!)
            }
            self.segFileRef = nil
            
            // Open an audio file and associate it with the extended audio file object.
            let sourceURL:CFURL = unsafeBitCast(self.mediaItem.assetURL, to: CFURL.self)
            
            var result:OSStatus = ExtAudioFileOpenURL(sourceURL, &(self.segFileRef))
            
            if noErr != result || nil == self.segFileRef {
                self.debugError("ExtAudioFileOpenURL:", status:result)
                return
            }
            
            var fileLengthFrames:Int64 = 0
            var propertySize:UInt32 = (UInt32)(MemoryLayout<Int64>.size)
            
            ExtAudioFileGetProperty(
                self.segFileRef!,
                kExtAudioFileProperty_FileLengthFrames,
                &propertySize,
                &fileLengthFrames)
            
            var fileDataFormat:AudioStreamBasicDescription = AudioStreamBasicDescription()
            propertySize = UInt32(MemoryLayout<AudioStreamBasicDescription>.size)
            
            ExtAudioFileGetProperty(
                self.segFileRef!,
                kExtAudioFileProperty_FileDataFormat,
                &propertySize,
                &fileDataFormat)
            
            self.duration = (Float)(fileLengthFrames)/(Float)(fileDataFormat.mSampleRate)
            
            let channelCount:UInt32 = fileDataFormat.mChannelsPerFrame
            
            var importFormat:AudioStreamBasicDescription
            if 2 == channelCount {
                importFormat = self.stereoStreamDescription
            } else if 1 == channelCount {
                importFormat = self.monoStreamDescription
            } else {
                self.amSegmentQueue.removeAll()
                ExtAudioFileDispose (self.segFileRef!)
                return
            }
            
            importFormat.mSampleRate = fileDataFormat.mSampleRate
            
            result = ExtAudioFileSetProperty (
                self.segFileRef!,
                kExtAudioFileProperty_ClientDataFormat,
                UInt32(MemoryLayout<AudioStreamBasicDescription>.size),
                &importFormat
            )
            
            result = AudioUnitSetProperty (
                self.mixerUnit!,
                kAudioUnitProperty_SampleRate,
                kAudioUnitScope_Input,
                0,
                &(fileDataFormat.mSampleRate),
                UInt32(MemoryLayout<Float64>.size)
            )
            
            if noErr != result {
                self.debugError("ExtAudioFileSetProperty (client data format):", status:result)
                return
            }
            self.renderMutex.lock()
            
            free(self.audioStruct.dataLeft)
            free(self.audioStruct.dataRight)
            self.audioStruct.dataLeft = nil
            self.audioStruct.dataRight = nil
            
            // Read all the data into memory
            self.audioStruct.isStereo = (2 == channelCount)
            self.audioStruct.totalFrame = Int64(fileLengthFrames)
            
            free(self.leftSeg)
            free(self.rightSeg)
            if self.bufferSeg != nil {
                free(self.bufferSeg.unsafeMutablePointer)
            }
            self.bufferSeg = AudioBufferList.allocate(maximumBuffers: Int(channelCount))
            
            // initialize the mBuffers member to 0
            //let emptyBuffer = AudioBuffer()
            for arrayIndex in 0..<Int(channelCount) {
                self.bufferSeg[arrayIndex] = AudioBuffer()
            }
            
            // set up the AudioBuffer structs in the buffer list
            self.audioStruct.dataLeft = UnsafeMutablePointer<Float>.allocate(capacity: Int(fileLengthFrames))
            
            let packetLength = 256*1024
            let packetSize = packetLength * MemoryLayout<Float>.stride
            
            self.leftSeg = UnsafeMutablePointer<Float>.allocate(capacity: packetLength)
            
            self.bufferSeg[0].mNumberChannels = 1
            self.bufferSeg[0].mDataByteSize = UInt32(packetSize)
            self.bufferSeg[0].mData = UnsafeMutableRawPointer(self.leftSeg)
            
            if channelCount == 2 {
                self.audioStruct.dataRight = UnsafeMutablePointer<Float>.allocate(capacity: Int(fileLengthFrames))
                
                self.rightSeg = UnsafeMutablePointer<Float>.allocate(capacity: packetLength)
                
                self.bufferSeg[1].mNumberChannels = 1
                self.bufferSeg[1].mDataByteSize = UInt32(packetSize)
                self.bufferSeg[1].mData = UnsafeMutableRawPointer(self.rightSeg)
            }
            
            self.amSegment.start = 0
            self.amSegment.pos = 0
            self.amSegment.end = fileLengthFrames
            
            self.renderMutex.unlock()
            
            self.executeReadSeg = {
                self.fileMutex.lock()
                
                if self.segFileRef == nil { return }
                
                result = ExtAudioFileSeek(self.segFileRef!, self.amSegment.pos)
                
                if self.amSegment.pos < self.amSegment.end {
                    
                    var packetToRead:UInt32 = UInt32(min((self.amSegment.end-self.amSegment.pos), packetLength))
                    
                    
                    result = ExtAudioFileRead(
                        self.segFileRef!,
                        &packetToRead,
                        self.bufferSeg.unsafeMutablePointer
                    )
                    
                    if noErr != result {
                        self.debugError("ExtAudioFileRead failure - :", status:result)
                        free(self.bufferSeg[0].mData)
                        if self.audioStruct.isStereo {
                            free(self.bufferSeg[1].mData)
                        }
                        free(self.bufferSeg.unsafeMutablePointer)
                        self.renderMutex.lock()
                        
                        free(self.audioStruct.dataLeft)
                        free(self.audioStruct.dataRight)
                        
                        self.audioStruct.dataLeft = nil
                        self.audioStruct.dataRight = nil
                        self.audioStruct.currentFrame = 0
                        
                        self.renderMutex.unlock()
                        
                        self.isLoading = false
                        
                        if self.segFileRef != nil {
                            self.amSegmentQueue.removeAll()
                            ExtAudioFileDispose(self.segFileRef!)
                            self.segFileRef = nil
                        }
                        return
                    }
                    
                    if self.isLoading && (self.amSegment.pos > self.amSegment.start) { //buffer end
                        self.audioStruct.currentFrame = 0
                        self.isLoading = false
                    }
                    
                    let advancedBy = Int(self.amSegment.pos)
                    let packetRead = Int(packetToRead)
                    
                    self.audioStruct.dataLeft?.advanced(by: advancedBy).assign(from: self.leftSeg!, count:packetRead)
                    
                    if self.audioStruct.isStereo {
                        self.audioStruct.dataRight?.advanced(by: advancedBy).assign(from: self.rightSeg!, count:packetRead)
                    }
                    
                    self.amSegment.pos += Int64(packetRead)
                    
                }
                
                if self.amSegment.pos < self.amSegment.end {
                    DispatchQueue.global().async(execute: self.executeReadSeg)
                }else{
                    if !self.amSegmentQueue.isEmpty {
                        self.amSegment = self.amSegmentQueue.dequeue()!
                        DispatchQueue.global().async(execute: self.executeReadSeg)
                    }else{
                        ExtAudioFileDispose(self.segFileRef!)
                    }
                }
                
                self.fileMutex.unlock()
            }
            
            DispatchQueue.global().async(execute: self.executeReadSeg)
            
            self.fileMutex.unlock()
        })
    }
    
    fileprivate func startAUGraph(){
        let result = AUGraphStart(self.augraph!)
        if noErr != result {
            debugError("AUGraphStart:", status:result)
            return
        }
        
        self.isPlaying = true
    }
    fileprivate func stopAUGraph(){
        var isRunning:DarwinBoolean = false
        var result = AUGraphIsRunning(self.augraph!, &isRunning)
        if noErr != result {
            debugError("AUGraphIsRunning:", status:result)
            return
        }
        
        if isRunning.boolValue {
            result = AUGraphStop (self.augraph!)
            if noErr != result {
                debugError("AUGraphStop:", status:result)
                return
            }
            self.isPlaying = false
        }
    }
    
    fileprivate func processSetFrame(_ frame:Int){
        let newPos = Int64(frame)
        
        self.fileMutex.lock()
        if self.amSegment.containsAtFront(newPos) {
            //loading not require
        }else if self.amSegment.containsAtBack(newPos){
            let seg = AMSegment(start: self.amSegment.pos, end: newPos)
            
            self.amSegmentQueue.enqueue(segment: seg)
            
            self.amSegment.pos = newPos
        }else{
            let index = self.amSegmentQueue.getSegmentIndexContains(newPos)
            if index > -1 {
                let seg = AMSegment(start:self.amSegment.pos, end:self.amSegment.end)
                let update = self.amSegmentQueue.getSegmentAt(index)
                
                self.amSegment.start = newPos
                self.amSegment.pos = newPos
                self.amSegment.end = update!.end
                
                update!.end = newPos
                self.amSegmentQueue.updateSegment(at: index, with:update!)
                self.amSegmentQueue.enqueue(segment: seg)
            }
        }
        
        self.fileMutex.unlock()
    }
    
    /* AURenderCallback */
    fileprivate let auRenderCallback:AURenderCallback = {
        (
        inRefCon: UnsafeMutableRawPointer,
        ioActionFlags: UnsafeMutablePointer<AudioUnitRenderActionFlags>,
        inTimeStamp: UnsafePointer<AudioTimeStamp>,
        inBusNumber: UInt32,
        inNumberFrames: UInt32,
        ioData: UnsafeMutablePointer<AudioBufferList>?
        ) -> OSStatus in
        
        let mixer:AMMixerHost = unsafeBitCast(inRefCon, to: AMMixerHost.self)
        let abl:UnsafeMutableAudioBufferListPointer? = UnsafeMutableAudioBufferListPointer(ioData)
        
        // Get the sample number, as an index into the sound stored in memory,
        // to start reading data from.
        var sampleNumber = mixer.audioStruct.currentFrame
        
        if mixer.isLoading {
            sampleNumber = 0
            for buffer:AudioBuffer in abl! {
                let buf:UnsafeMutablePointer<Float> = (buffer.mData?.assumingMemoryBound(to: Float.self))!
                for i:Int in 0 ..< Int(inNumberFrames) {
                    buf[i] = 0
                }
            }
            sampleNumber = inNumberFrames
            return noErr
        }
        // update current song time, send time to delegate
        mixer.renderMutex.lock()
        
        if mixer.delegate != nil {
            let currentTime:Float = ( (Float)(sampleNumber)/(Float)(mixer.audioStruct.totalFrame)) * mixer.duration
            mixer.delegate!.updateCurrentTime(currentTime)
        }
        
        var dataInL:UnsafeMutablePointer<Float>? = mixer.audioStruct.dataLeft
        var dataInR:UnsafeMutablePointer<Float>? = nil
        var dataOutL:UnsafeMutablePointer<Float>? = (abl?[0].mData?.assumingMemoryBound(to: Float.self))
        var dataOutR:UnsafeMutablePointer<Float>? = nil
        
        if mixer.audioStruct.isStereo {
            dataInR = mixer.audioStruct.dataRight
            dataOutR = (abl?[1].mData?.assumingMemoryBound(to: Float.self))
        }
        
        if mixer.delegate != nil && 0 == mixer.audioStruct.currentFrame {
            mixer.delegate!.onStart()
        }
        
        var sampleSize = sampleNumber + inNumberFrames
        if Int64(sampleSize) >= mixer.audioStruct.totalFrame {
            sampleSize = UInt32(mixer.audioStruct.totalFrame)
        }
        sampleSize -= sampleNumber
        
        var sampleBuffer:SampleBuffer = SampleBuffer(sampleSize:0, audioDataLeft: [], audioDataRight: [], isStereo: mixer.audioStruct.isStereo)
        sampleBuffer.sampleSize = Int(sampleSize)
        sampleBuffer.audioDataLeft = [Float](repeating: 0, count: sampleBuffer.sampleSize)
        if sampleBuffer.isStereo {
            sampleBuffer.audioDataRight = [Float](repeating: 0, count: sampleBuffer.sampleSize)
        }
        
        for frameNumber:Int in 0 ..< Int(inNumberFrames) {
            if Int64(sampleNumber) >= mixer.audioStruct.totalFrame {
                sampleNumber = UInt32(mixer.audioStruct.totalFrame)
                break
            }
            
            if dataInL != nil {
                dataOutL?[frameNumber] = (dataInL?[Int(sampleNumber)])!
                sampleBuffer.audioDataLeft[frameNumber] = (dataInL?[Int(sampleNumber)])!
            }
            
            if mixer.audioStruct.isStereo {
                if dataInR != nil {
                    dataOutR?[frameNumber] = (dataInR?[Int(sampleNumber)])!
                    sampleBuffer.audioDataRight[frameNumber] = (dataOutR?[frameNumber])!
                }
            }
            sampleNumber+=1
        }
        
        mixer.renderMutex.unlock()
        
        if mixer.delegate != nil {
            mixer.delegate!.handleSample(sampleBuffer)
        }
        
        // Update the stored sample number so, the next time this callback is invoked, playback resumes
        //    at the correct spot.
        mixer.audioStruct.currentFrame = sampleNumber
        
        if mixer.delegate != nil && Int64(sampleNumber) == mixer.audioStruct.totalFrame {
            mixer.delegate!.onFinish()
        }
        return noErr
    }
    
    
    /* public property */
    var currentMediaItem:MPMediaItem?{
        get{
            return self.mediaItem
        }
        set(mediaItem){
            if self.isLoading { return }
            if self.mediaItem != nil && self.mediaItem.assetURL == mediaItem?.assetURL { return }
            self.mediaItem = mediaItem
            self.isLoading = true
            self.readSegments()
        }
    }
    
    fileprivate(set) var isPlaying:Bool = false
    
    /* public function */
    func start(_ forced:Bool = false){
        if self.isPlaying && !forced { return }
        if self.isPlaying { self.stopAUGraph() }
        self.startAUGraph()
    }
    
    func stop(){
        if !self.isPlaying { return }
        
        self.stopAUGraph()
    }
    
    func setTime(_ time:Float){
        // if graph is isLoading new song, just ignore the request
        if self.isLoading || self.duration <= 0 {
            return
        }
        
        // set time for the song
        if time >= 0 || time < self.duration {
            audioStruct.currentFrame = (UInt32)((Float)(self.audioStruct.totalFrame) * (time / self.duration))
            self.processSetFrame(Int(audioStruct.currentFrame))
        }
    }
    func getTime() -> Float {
        if self.audioStruct.totalFrame <= 0 {
            return 0
        }
        return self.getPercentage() * self.duration
    }
    func setPercentage(_ percentage:Float){
        self.setTime(percentage*self.duration)
    }
    func getPercentage() -> Float {
        return (Float)(self.audioStruct.currentFrame)/(Float)(self.audioStruct.totalFrame)
    }
    func setEQGlobalGain(_ gainValue:Float){
        let result = AudioUnitSetParameter(
            eqUnit!,
            kAUNBandEQParam_GlobalGain,
            kAudioUnitScope_Global,
            0,
            gainValue,
            0)
        
        if noErr != result {
            debugError("AudioUnitSetParameter/BandEQ_GlobalGain/Global", status:result)
            return
        }
        
        self.eqGlobalGain = gainValue
    }
    func setEQGain(frequencyTag:Int,gainValue:Float){
        // To set a parameter for a band you need to add the band number to the revelant enum for that parameter
        let parameterID:AudioUnitParameterID = kAUNBandEQParam_Gain + UInt32(frequencyTag)
        let result = AudioUnitSetParameter(
            eqUnit!,
            parameterID,
            kAudioUnitScope_Global,
            0,
            gainValue,
            0)
        
        if noErr != result {
            debugError("AudioUnitSetParameter/BandEQ_Gain/Global", status:result)
            return
        }
        
        self.eqFrequencyGain[frequencyTag] = gainValue
    }
    func setEQPreset(_ presetIndex:Int) -> Int {
        if self.factoryPreset == nil { return 0 }
        
        var index = presetIndex
        if index >= self.factoryPresetCount || index < 0 {
            index = 0
        }
        let preset = CFArrayGetValueAtIndex(self.factoryPreset?.pointee, index)
        let ioDataSize = UInt32(MemoryLayout<AUPreset>.size)
        let result:OSStatus = AudioUnitSetProperty(ipodUnit!, kAudioUnitProperty_PresentPreset, kAudioUnitScope_Global, 0, preset, ioDataSize)
        
        if noErr != result {
            debugError("AudioUnitSetProperty/PresentPreset/Global", status: result)
            return 0
        }
        
        return index
    }
    func resetEQ(){
        var result:OSStatus
        result = AudioUnitSetParameter(
            eqUnit!,
            kAUNBandEQParam_GlobalGain,
            kAudioUnitScope_Global,
            0,
            0,
            0)
        if noErr != result {
            debugError("AudioUnitSetParameter/BandEQ_GlobalGain/Global", status:result)
        }
        for i in 0..<self.eqFrequencyBand.count {
            result = AudioUnitSetParameter(
                eqUnit!,
                kAUNBandEQParam_Gain + UInt32(i),
                kAudioUnitScope_Global,
                0,
                (AudioUnitParameterValue)(0),
                0)
            self.eqFrequencyGain[i] = 0
            if noErr != result {
                debugError("AudioUnitSetParameter/BandEQ_Gain/Global", status:result)
                return
            }
        }
    }
    func getIpodPresets() -> [String] {
        if self.factoryPreset == nil { return [] }
        
        var presetNames:[String] = []
        
        for idx in 0..<self.factoryPresetCount {
            let presetRaw = CFArrayGetValueAtIndex(self.factoryPreset?.pointee, idx)
            let preset:AUPreset = (presetRaw?.assumingMemoryBound(to: AUPreset.self).pointee)!
            
            let presetName:String? = AMUtils.convertCfTypeToString(cfValue: preset.presetName)
            if presetName != nil {
                presetNames.append(presetName!)
            }
        }
        
        return presetNames
    }
    func getFrequencyBand() -> [Float] {
        return self.eqFrequencyBand
    }
    func getFrequencyGain() ->[Float]{
        return self.eqFrequencyGain
    }
    func getGlobalGain() -> Float {
        return self.eqGlobalGain
    }
    
    func setDelegate(_ delegate:AMMixerHostDelegate){
        self.delegate = delegate
    }
    
    fileprivate func debugError(_ error:Error){
        print(error)
    }
    
    fileprivate func debugError(_ message:String, status:OSStatus){
        print(message, "\nerror:", status)
    }
}
