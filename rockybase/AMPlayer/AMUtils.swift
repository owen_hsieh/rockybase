import UIKit

open class AMUtils {
    
    class func resizeImage(image:UIImage, newWidth:CGFloat) -> UIImage {
        let scale = newWidth / image.size.width
        
        if scale == 1 {
            return image
        }
        
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width:newWidth, height:newHeight))
        image.draw(in: CGRect(x:0, y:0, width:newWidth, height:newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    
    class func convertCfTypeToString(cfValue: Unmanaged<CFString>!) -> String?{
        let value = Unmanaged<CFString>.fromOpaque(
            cfValue.toOpaque()).takeUnretainedValue() as CFString
        if CFGetTypeID(value) == CFStringGetTypeID(){
            return value as String
        } else {
            return nil
        }
    }
}
