//
//  Sync.swift
//
//  Created by owen on 2016/10/5.
//  Copyright © 2016年 Ampacs. All rights reserved.
//
//  Just some easier understand sync
//

import Foundation

open class Mutex {
    fileprivate var name:String
    fileprivate var mutex = pthread_mutex_t()
    
    init(_ name:String = "") {
        self.name = name
        pthread_mutex_init(&mutex, nil)
    }
    
    deinit {
        pthread_mutex_destroy(&mutex)
    }
    
    public func lock(){
        if !(name.isEmpty) {
            print("trylock:", name)
        }
        pthread_mutex_lock(&mutex)
        if !(name.isEmpty) {
            print("locked", name)
        }
    }
    
    public func unlock(){
        if !(name.isEmpty) {
            print("unlock:", name)
        }
        pthread_mutex_unlock(&mutex)
    }
}
