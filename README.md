# README #

此App分為4個功能頁

* Indicator

    ![Indicator示意圖](http://i.imgur.com/tL2geYO.png)

    包含

    1. ANC開關

    2. dB比較

    3. dB比較(30秒)

    4. 預定：各頻率dB比較

* Settings

    ![Settings示意圖](http://i.imgur.com/oNSHiut.png)


* Equalizer
    * 等化器 (可選擇 preset 或是手動調整數值)

* Player
    * 內建播放器(目前放了一個預設版本，希望可以美化)


UI流程目前附於rockybase/rockbase/Base.lproj/Main.storyboard
